;;; guix.scm --- Get the most recent fully evaluated commit from the build farm

;; Copyright © 2018 Oleg Pykhalov <go.wigust@gmail.com>

;; Emacs-Guix-Misc is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Guix-Latest-Eval is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Guix-Latest-Eval.
;; If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains Guix package for development version of
;; Guix-Latest-Eval.  To build or install, run:
;;
;;   guix build --file=guix.scm
;;   guix package --install-from-file=guix.scm

;;; Code:

(use-modules ((guix licenses) #:prefix license:)
             (guix build utils)
             (guix build-system trivial)
             (guix gexp)
             (guix git-download)
             (guix packages)
             (gnu packages bash)
             (gnu packages curl)
             (ice-9 popen)
             (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define (git-output . args)
  "Execute 'git ARGS ...' command and return its output without trailing
newspace."
  (with-directory-excursion %source-dir
    (let* ((port   (apply open-pipe* OPEN_READ "git" args))
           (output (read-string port)))
      (close-port port)
      (string-trim-right output #\newline))))

(define (current-commit)
  (git-output "log" "-n" "1" "--pretty=format:%H"))

(let ((commit (current-commit)))
  (package
    (name "guix-latest-eval")
    (version (string-append "0.0.1" "-" (string-take commit 7)))
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (build-system trivial-build-system)
    (inputs
     `(("bash" ,bash)
       ("curl" ,curl)))
    (arguments
     `(#:modules
       ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (copy-recursively (assoc-ref %build-inputs "source") ".")
         (setenv "PATH" (string-append
                         (assoc-ref %build-inputs "bash") "/bin" ":"
                         (assoc-ref %build-inputs "curl") "/bin" ":"))
         (substitute* "guix-latest-eval"
           (("/bin/sh") (which "bash"))
           (("curl") (which "curl")))
         (let ((directory "bin"))
           (install-file "guix-latest-eval"
                         (string-append %output "/" directory)))
         #t)))
    (home-page #f)
    (synopsis "Get the most recent fully evaluated commit from the build farm")
    (description
     "This package provides a Curl script to get the most recent fully
evaluated commit from the build farm.")
    (license license:gpl3+)))

;;; guix.scm ends here
